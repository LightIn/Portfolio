const hamburger = document.querySelector('.hamburger');
const navLinks = document.querySelector('.nav_links');
const links = document.querySelectorAll('.nav_links li');


hamburger.addEventListener("click", () => {
    navLinks.classList.toggle("open");
    if (navLinks.classList.contains("open") ){
        navLinks.style.display = "flex";
    } else {
        navLinks.style.display = "none";
    }
});